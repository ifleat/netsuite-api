/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       30 Sep 2016     Vlaszaty
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

	return "Hello Customer";
}

/**
 * @param {Object} dataIn Parameter object
 *
 * datain contains:
 * 		- updated customer name
 * 		- customer email
 * 		- order nrs of the orders that should be adjusted
 *
 * @returns {Object} Output object
 */
function postCustomerDetails(dataIn) {

	nlapiLogExecution('DEBUG',"input received", JSON.stringify(dataIn));

	var result = new Object();
	var customer_id = null;

	// check if all parameters are present
	if ((!	dataIn.hasOwnProperty('email_address') &&
			dataIn.hasOwnProperty('order_nrs'))) {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, provided input does not include all required fields";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		return result;
	}

	// check if name field is not empty
	if (dataIn.name == "") {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, name field is empty";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		return result;
	}

	// check if email field is not empty
	if (dataIn.email == "") {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, email field is empty";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		return result;
	}

	// check if order_nrs list contains 1 or 2 salesorders
	if (dataIn.order_nrs.length == 0 || dataIn.order_nrs.length > 2) {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, length of order_nrs list is either 0 or more then 2";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		return result;
	}

	// check if order nrs are valid

	//check if order nrs belong to the same customer


	// check if customer with this email address already exists
	var matching_customer = nlapiSearchRecord('customer', null, new nlobjSearchFilter('email', null, 'is', dataIn.email_address)); //Should filter on both


	// if not, update the current customer profile with this email address
	if (matching_customer == null) {
		nlapiLogExecution('DEBUG',"email address unknown", dataIn.email_address);
		//email address is new to us, Fill in this address in the existing customer

		//for all the orders find the customer and add the given email address
		for (var oidx in dataIn.order_nrs){
			var order_nr = dataIn.order_nrs[oidx];

			var order = nlapiLoadRecord('salesorder', order_nr);
			var order_customer_id = order.getFieldValue('entity');
			var customer = nlapiLoadRecord('customer', order_customer_id);

			customer.setFieldValue('email', dataIn.email_address);
			nlapiSubmitRecord(customer);
		}
	}


	// if there is a customer with this email address, this is the customer that should be linked to the order and the customer deposit
	else if (matching_customer.length == 1) {

		nlapiLogExecution('DEBUG',"email address already known", dataIn.email_address);
		// Get the id of the customer already with this email address
		customer_id = matching_customer[0].getId();

		// Link salesorders and its customer deposit to the different customer
		for (var oidx in dataIn.order_nrs){
			var order_nr = dataIn.order_nrs[oidx];


			var order = nlapiLoadRecord('salesorder', order_nr);
			var customer_deposit  = null;

			//find customer deposit that is connected to this sales order
			var matching_customer_deposit = nlapiSearchRecord('customerdeposit', null, new nlobjSearchFilter('salesorder', null, 'is', order_nr));


			if (matching_customer_deposit == null || (!matching_customer_deposit.length == 1)) {
				result.error = new Object();
				result.error.status = "INTERNAL_ERROR";
				result.error.message = "Error in determining the customer deposit linked to salesorder " + order_nr;
				nlapiLogExecution('DEBUG',result.error.status, result.error.message);
				return result;
			}


			else {
				customer_deposit = nlapiLoadRecord('customerdeposit', matching_customer_deposit[0].getId());
			}
			
			nlapiLogExecution('DEBUG',"Setting order entity to customer id ", customer_id);

			order.setFieldValue('entity', customer_id);
			nlapiSubmitRecord(order);


			nlapiLogExecution('DEBUG',"Setting deposit customer field to customer id ", customer_id);
			nlapiLogExecution('DEBUG',"customer_deposit ", JSON.stringify(customer_deposit));
			customer_deposit.setFieldValue('customer', customer_id);
			customer_deposit.setFieldValue('account', customer_deposit.getFieldValue('account')); // solves user error regarding accounts
			customer_deposit.setFieldValue('salesorder', customer_deposit.getFieldValue('salesorder')); // solves linking error
			nlapiSubmitRecord(customer_deposit);

		}
	}

	else {
		result.error = new Object();
		result.error.status = "CUSTOMER_ERROR";
		result.error.message = "Customer error, length of matching_customer list more then 1";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		return result;
	}

    result.success = new Object();
    result.success.message = "Succesfully added email address " +dataIn.email_address+" to orders " + dataIn.order_nrs.toString();
    result.success.id = dataIn.email_address;
    nlapiLogExecution('DEBUG',result.success.message, result.success.id);
    return result;
}
