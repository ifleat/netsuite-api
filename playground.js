/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2017     Vlaszaty
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

	var context = nlapiGetContext().getEnvironment();

	if(context == 'SANDBOX') {
		var user = 'sanboxuser';
	}
	else {
		var user = 'otheruser';
	}


	return user;
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

	return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Void}
 */
function deleteRESTlet(dataIn) {

}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function putRESTlet(dataIn) {

	return {};
}
