//master password: netsuite2016

// Get a standard NetSuite record
function getRecord(datain)
{
	var id = 3;
    var record =  nlapiLoadRecord('employee', id); // e.g recordtype="customer", id=769
    return JSON.stringify(record);
}

// input should be string like "MUSTERMANN/PHIL/MR"
// output is either Mr or Mrs 
function salutation_from_string(input) {
	var s = input.split("/")[2];
	return s[0].toUpperCase() + s.substring(1).toLowerCase()
}



function extract_mr_mrs(datain_message) {
	
	var result = "Mr/Mrs."
	
	var d = datain_message;
	

	// if number of passengers in flight is 1, use that persons details
	
	
	if (d.flight_segments.length == 1) {
		var s = d.flight_segments[0];
		
		//When there is 1 order in 
		if (s.order.length == 1) {
			return salutation_from_string(s.order[0].name)
		}
		else {
			
		}
	}
	
	
	
	
	// if multiple passengers, match datain.last_name to passengers in booking to find candidates
	
	// if multiple candidates, but all are same gender, we can use either Mr. or Mrs. 
	
	// else we have no other option than to use Mr/Mrs
}

function stations_to_route(departure_station, arrival_station) {
	// TODO: validate that input is properly formatted
	departure_station 	= departure_station.split("-")[0].trim();
	arrival_station 	= arrival_station.split("-")[0].trim();
	
	var routeString = departure_station + " - " + arrival_station;
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', routeString);
	 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn( 'internalid' );

	var search_routes = nlapiSearchRecord( 'customrecord_routes', null, filters, columns );
	if(search_routes != null)
	{
	    return routeId = search_routes[0].getValue(columns[0]);
	}
	else {
		nlapiLogExecution('ERROR','Route not found', routeString);
	}
	
}

/**
 * Validates presence of all fields required to do an update
 * @param datain
 * @returns {Boolean}
 */ 
function order_update_input_ok(datain) {
	
	if (datain.hasOwnProperty('order_id') &&
			datain.hasOwnProperty('carrier') &&
			datain.hasOwnProperty('flight_number')&&
			datain.hasOwnProperty('departure_date') &&
			datain.hasOwnProperty('departure_time') &&
			datain.hasOwnProperty('flight_time')){
		return true;
	}
	else {
		return false;
	}
}

/**
 * Validates if all necessary fields are present in the input data
 * @param datain
 * @returns {Boolean}
 */
function order_create_input_ok(datain) {
	
	if (datain.hasOwnProperty('booking_reference') &&
			datain.hasOwnProperty('last_name') &&
			datain.hasOwnProperty('email_address')&&
			datain.hasOwnProperty('order_timestamp') &&
			datain.hasOwnProperty('flight_segments') &&
			datain.hasOwnProperty('payment_details') &&
			flight_segments_ok(datain.flight_segments) &&
			payment_details_ok(datain.payment_details)){
		return true;
	}
	else {
		return false;
	}

}


/**
 * Validates if all necessary fields are present in the flight_segments.
 * Used in order_input_ok
 * @param datain
 * @returns {Boolean}
 */
function flight_segments_ok(segments) {
	
	for (var i = 0; i < segments.length; i++) {
        var s = segments [i];
	
	    if (!(s.hasOwnProperty('carrier') &&
		    s.hasOwnProperty('flight_number') &&
	        s.hasOwnProperty('departure_date') &&
		    s.hasOwnProperty('departure_time') &&
		    s.hasOwnProperty('flight_time') &&
		    s.hasOwnProperty('departure_location') &&
		    s.hasOwnProperty('arrival_location') &&
		    s.hasOwnProperty('order')
		    )) {
	    return false;
	    }
	}

    return true;
}

/**
 * Validates if all necessary fields are present in the payment details
 * Used in order_input_ok
 * @param datain
 * @returns {Boolean}
 */
function payment_details_ok(datain) {
	if (datain.hasOwnProperty('country_code') &&
		datain.hasOwnProperty('creation_time') &&
		datain.hasOwnProperty('payment_amount') &&
		datain.hasOwnProperty('currency_code') &&
		datain.hasOwnProperty('merchant_reference')) {
		return true;
		
	}
	else {
		return false;
	}
}

/**
 * 
 * @param datain
 * 
 * TODO:
 * 	Department field in salesorder, how can i look that up?
 *	route field, how can i find the correct route?
 *
 *	create customer deposit to go with order
 *	What status should a salesorder have by default as it is essentially already payed	
 */
function createRecord(datain) {
	
	var result = new Object();
	var order_ids = [];
	var undefined_first_name_string = 'Mr/Mrs';
	
	if (!order_create_input_ok(datain)) {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, provided input does not include all required fields";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		
		return result;
	};
	
	//check if all ordered meal numbers actually exist
	for (var fidx in datain.flight_segments) {
		var segment = datain.flight_segments[fidx];
		for (var oidx in segment.order){
			var order = segment.order[oidx];
			
			if (order.product_nr == "") {
				result.error = new Object();
				result.error.status = "INPUT_ERROR";
				result.error.message = "Empty meal id detected for: "+ order.name;
				nlapiLogExecution('DEBUG',result.error.status, result.error.message);
				return result;
			}
			
			var kititem = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('internalid', null, 'anyOf', order.product_nr));
			if (kititem == null) {
				result.error = new Object();
				result.error.status = "INPUT_ERROR";
				result.error.message = "Unknown meal id detected: "+ order.product_nr;
				nlapiLogExecution('DEBUG',result.error.status, result.error.message);
				return result;
				
			}
		}
	}
	
	
	var customer_id = null;
	
	// if email is empty we can never allocate the order to a specific person. 
	if(!datain.email_address == "") {
	
		var matching_customer = nlapiSearchRecord('customer', null, new nlobjSearchFilter('email', null, 'is', datain.email_address)); //Should filter on both 
		
		if (matching_customer == null) {
			//create customer
			var new_customer = nlapiCreateRecord("customer");
			new_customer.setFieldValue('email', datain.email_address);
			new_customer.setFieldValue('lastname', datain.last_name);
			var firstname = (datain.firstname == null) ? undefined_first_name_string : datain.firstname;
			new_customer.setFieldValue('firstname', firstname);
	
			//set customer_id to the id of newly created customer
			customer_id = nlapiSubmitRecord(new_customer);
		}
		else if (matching_customer.length == 1) {
			customer_id = matching_customer[0].getId();
		}

		else {
			nlapiLogExecution('DEBUG','mathcing_customer length', matching_customer.length);
			return "customer creation error";
		}
	}
	
	// if there is no email address, always create new customer
	else {
		//create customer
		var new_customer = nlapiCreateRecord("customer");
		new_customer.setFieldValue('email', datain.email_address);
		new_customer.setFieldValue('lastname', datain.last_name);
		new_customer.setFieldValue('isperson', "T");
		var firstname = (datain.firstname == null) ? undefined_first_name_string : datain.firstname;
		new_customer.setFieldValue('firstname', firstname);

		//set customer_id to the id of newly created customer
		customer_id = nlapiSubmitRecord(new_customer);
	}
	
	nlapiLogExecution('DEBUG','customer ', customer_id);
	
	if (datain.flight_segments.length > 2 || datain.flight_segments.length == 0) {
		nlapiLogExecution('DEBUG','customer id', customer_id);
		return "Error: flight_segments should contain 1 or 2 segments.";
	}
	
	// Create sales order for every flight segment that contains at least one order
	for (var sidx in datain.flight_segments) {
		var segment = datain.flight_segments[sidx];
		
		if ( segment.order == null || segment.order.length == 0) {	
			result.error = new Object();
			result.error.status = "INPUT_ERROR";
			result.error.message = "Found segment without any orders";
			nlapiLogExecution('DEBUG',result.error.status, result.error.message);
			return result;
		}
		
		var new_order = nlapiCreateRecord('salesorder');
		
		// determine the departure and arrival location
		var departure_search = nlapiSearchRecord('location', null, new nlobjSearchFilter('name', null, 'is', segment.departure_location));
		var arrival_search = nlapiSearchRecord('location', null, new nlobjSearchFilter('name', null, 'is', segment.arrival_location));
		
		if(departure_search == null || arrival_search == null ||departure_search.length > 1 || arrival_search.length > 1) {
			
			result.error = new Object();
			result.error.status = "INPUT_ERROR";
			result.error.message = "Error in one or more of the given locations: " + segment.departure_location +", " + segment.arrival_location;
			nlapiLogExecution('DEBUG',result.error.status, result.error.message);
			
			return result;
		}
		
		var departure_location_id 	= departure_search[0].getId();
		var arrival_location_id 	= arrival_search[0].getId();
		
		// determine route
		var route = stations_to_route(segment.departure_location, segment.arrival_location);
		
		
		
		//fill in all fields
		nlapiLogExecution('DEBUG','filling in fields', sidx);
		new_order.setFieldValue('department', 1); //AB production = 1, in sandbox AB is 4
		new_order.setFieldValue('entity', customer_id);
		new_order.setFieldValue('location', departure_location_id); 
		new_order.setFieldValue('custbody_destination', arrival_location_id); 
		new_order.setFieldValue('custbody_flight_date', segment.departure_date);
		new_order.setFieldValue('saleseffectivedate', segment.departure_date);
		new_order.setFieldValue('custbody_flight_number', segment.carrier.concat(segment.flight_number));
		new_order.setFieldValue('custbody_route', route);
		
		for (var oidx in segment.order){
			var order = segment.order[oidx];
			
			new_order.selectNewLineItem('item');
			new_order.setCurrentLineItemValue('item','item', order.product_nr); // currently takes product number, should take external id
			new_order.setCurrentLineItemValue('item','taxcode', 5); // UNDEF-NL taxcode 
			new_order.commitLineItem('item'); 
		}
		
		var order_id = nlapiSubmitRecord(new_order);
		order_ids.push({"id":order_id, "ref": segment.ref});
		
		
		// create customer deposit for the orders
		nlapiLogExecution('DEBUG','creating deposit', sidx);
		var cust_deposit = nlapiCreateRecord('customerdeposit');
		cust_deposit.setFieldValue('customer', customer_id);
		cust_deposit.setFieldValue('salesorder', order_id);
		cust_deposit.setFieldValue('paymentmethod', 4); // references to Adyen in sandbox is 4, prod is also 4
		cust_deposit.setFieldValue('payment', nlapiLoadRecord('salesorder', order_id).getFieldValue('origtotal'));
		cust_deposit.setFieldValue('custbody_merchant_reference', datain.payment_details.merchant_reference);
		
		var deposit_id = nlapiSubmitRecord(cust_deposit);
	};
	
	result.succes = new Object();
	result.succes.order_ids = order_ids;
	
	return result;
}


/**
 * 
 * datain format:
 *  { "order_id": [Int] (ID of the order that needs to be changed)
 *    "carrier": [String] (IATA carrier code),
 *    "flight_number": [Int] (Flight number),
 *    "departure_date": [String] (Date the flight will take place (local time)),
 *    "departure_time": [String] (Time of the flight (local time)),
 *    "flight_time": [String] (Duration of the flight)
 *  }
 * @param datain
 * @returns
 */
function updateOrder(datain) {
	
	var result = new Object();
	
	if (!order_update_input_ok(datain)) {
		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Provided input does not contain all necessary information";
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		
		return result;
	}
	
	var order = nlapiLoadRecord('salesorder', datain.order_id);
	
	if (order == null) {
		result.error = new Object();
		result.error.status = "RECORD_NOT_FOUND";
		result.error.message = "Record with given order_id not found: "+ datain.order_id;
		nlapiLogExecution('DEBUG',result.error.status, result.error.message);
		
		return result;
	}
	
	order.setFieldValue('location', datain.departure_location);
	order.setFieldValue('custbody_destination', datain.arrival_location);
	
	var recordId = nlapiSubmitRecord(order);
	nlapiLogExecution('DEBUG','id='+recordId);
    
    result.success = new Object();
    result.success.message = "Record succesfully edited";
    result.success.id = recordId;
    nlapiLogExecution('DEBUG',result.success.message, result.success.id);
    return result;
}

