/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       01 Aug 2016     Vlaszaty
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getMeals(dataIn) {

	return "Meal API";
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 *
 * Expects a datain object with the fields:
 * 'name' : Name of the meal
 * 'externalid' : id of the meal from CMS
 *
 * {'externalid': 1234,
 *  'name': 'nice meal',
 *  'price_eur_cents': 1200}
 *
 */
function createMeal(datain) {

	var result = new Object();

	if (!datain.hasOwnProperty('externalid') ||
		!datain.hasOwnProperty('name') ||
		!datain.hasOwnProperty('station') ||
		!datain.hasOwnProperty('price_eur_cents')) {

		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, provided input does not include externalid and name fields";
		return result;
	}

	// Check if the station actually exists
	var station_search = nlapiSearchRecord('location', null, new nlobjSearchFilter('name', null, 'is', datain.station));
	if(station_search == null) {
		result.error = new Object();
		result.error.status = "STATION_ERROR";
		result.error.message = "Given station not found: "+datain.station;
		return result;
	}


	// check if a meal with this name doesn't already exist
	var dupNameSearchResult = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('itemid', null, 'is', datain.name));
	if (!(dupNameSearchResult == null)) {
		result.error = new Object();
		result.error.status = "DUP_ERROR";
		result.error.message = "A meal with this name already exists.";

		return result;
	}

	var dupExIdSearchResult = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('externalid', null, 'is', datain.externalid));
	if (!(dupExIdSearchResult == null)) {
		result.error = new Object();
		result.error.status = "DUP_ERROR";
		result.error.message = "A meal with this externalid already exists.";

		return result;
	}

	var item = nlapiCreateRecord('kititem');
	item.setFieldValue('itemid', datain.name);
	item.setFieldValue('description', datain.name+"-"+datain.station);
	item.setFieldValue('displayname', datain.name);
	item.setFieldValue('externalid', datain.externalid);
	item.setFieldValue('taxschedule', 1);
	item.setLineItemValue('price', 'price_1_', '1', datain.price_eur_cents/100);

	item.selectNewLineItem('member');
	item.setCurrentLineItemValue('member','item', 49);  //49 is the internalid for a 1/1 tray on production, 220 on sandbox
	item.commitLineItem('member');

	var recordId = nlapiSubmitRecord(item);

	//return recordId;
	nlapiLogExecution('DEBUG','Created meal with id='+recordId);

    result.success = new Object();
    result.success.message = "Record succesfully created";
    result.success.id = recordId;
    return result;
}

/**
 * Update meal, mainly used to update price
 * @param datain
 * @returns {___anonymous1861_1866}
 */

function updateMeal(datain) {
	nlapiLogExecution('DEBUG', "updating meal", "externalid: "+ datain.externalid );

	var result = new Object();

	if (!datain.hasOwnProperty('externalid') ||
		!datain.hasOwnProperty('name') ||
		!datain.hasOwnProperty('station') ||
		!datain.hasOwnProperty('price_eur_cents')) {

		result.error = new Object();
		result.error.status = "INPUT_ERROR";
		result.error.message = "Input error, provided input does not include all required fields";
		nlapiLogExecution('DEBUG', result.error.status, result.error.message);
		return result;
	}

	// Check if the station actually exists
	var station_search = nlapiSearchRecord('location', null, new nlobjSearchFilter('name', null, 'is', datain.station));
	if(station_search == null) {
		result.error = new Object();
		result.error.status = "STATION_ERROR";
		result.error.message = "Given station not found: "+datain.station;
		nlapiLogExecution('DEBUG', result.error.status, result.error.message);
		return result;
	}

	var inputItemId = datain.name +" "+ datain.station;

	var mealSearchResult = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('externalid', null, 'is', datain.externalid));

	// If no record is found with this externalid, return an error
	if (mealSearchResult == null) {
		result.error = new Object();
		result.error.status = "RECORD_NOT_FOUND";
		result.error.message = "Meal with that externalid not found: "+ datain.externalid;
		nlapiLogExecution('DEBUG', result.error.status, result.error.message);
		return result;
	}

	// If the search gives more then 1 result, return an error
	if (mealSearchResult.length > 1) {
		result.error = new Object();
		result.error.status = "INTERNAL_ERROR";
		result.error.message = "An internal error occured.";
		nlapiLogExecution('DEBUG', result.error.status, result.error.message);
		return result;
	}

	meal = nlapiLoadRecord('kititem',mealSearchResult[0].getId());

	// check if the name of the item has changed
	if(!meal.getFieldValue("itemid").equals(inputItemId)) {
		// check if a meal with the new name doesn't already exist
		// TODO: needs logic to cope with an unchanged name but a changed price
		var dupSearchResult = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('itemid', null, 'is', inputItemId));
		if (!(dupSearchResult == null)) {
			result.error = new Object();
			result.error.status = "DUP_ERROR";
			result.error.message = "A meal with this name already exists.";
			return result;
		}

		meal.setFieldValue('itemid', inputItemId);
	}

	meal.setFieldValue('description', datain.name+"-"+datain.station);
	meal.setFieldValue('displayname', datain.name);
	meal.setFieldValue('taxschedule', 1);

	// set price of the meal in eur
	meal.setLineItemValue('price', 'price_1_', '1', datain.price_eur_cents/100);

	var recordId = nlapiSubmitRecord(meal);
	nlapiLogExecution('DEBUG','id='+recordId);

    result.success = new Object();
    result.success.message = "Record succesfully edited";
    result.success.id = recordId;
    return result;
}

// Does not return any value, as per NetSuite specification
function deleteMeal(datain) {

	if (!datain.hasOwnProperty('externalid')) {
		nlapiLogExecution('DEBUG', "INPUT_ERROR", "Input error, provided input does not have an externalid field");
		return;
	}

	var mealSearchResult = nlapiSearchRecord('kititem', null, new nlobjSearchFilter('externalid', null, 'is', datain.externalid));

	if (mealSearchResult == null) {
		nlapiLogExecution('DEBUG', "RECORD_NOT_FOUND", "Meal with that externalid not found");
		return;
	}

	// If the search gives more then 1 result, return an error
	if (mealSearchResult.length > 1) {
		nlapiLogExecution('DEBUG', "INTERNAL_ERROR", "An internal error occured.");
		return;
	}

	var mealId = mealSearchResult[0].getId();

	nlapiDeleteRecord('kititem', mealId);
	nlapiLogExecution('DEBUG', 'Record deleted', datain.externalid);
}
